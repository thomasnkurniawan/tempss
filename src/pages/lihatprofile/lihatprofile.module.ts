import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LihatprofilePage } from './lihatprofile';

@NgModule({
  declarations: [
    LihatprofilePage,
  ],
  imports: [
    IonicPageModule.forChild(LihatprofilePage),
  ],
})
export class LihatprofilePageModule {}
