import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';

/**
 * Generated class for the LihatprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lihatprofile',
  templateUrl: 'lihatprofile.html',
})
export class LihatprofilePage {
  dataMember
  dataUser
  constructor(public navCtrl: NavController, public navParams: NavParams, public afd: AngularFireDatabase) {
    this.dataMember = this.navParams.get("b");
    console.log(this.dataMember , "ini data member");

    this.afd.list("users/", ref=> ref.orderByChild('uid').equalTo(this.dataMember.userId)).valueChanges().subscribe(data=>{
      this.dataUser = data;
      console.log(this.dataUser);
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LihatprofilePage');
  }

  back(){
    this.navCtrl.pop();
  }
}
