import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, Platform, ToastController, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage } from 'angularfire2/storage';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})


export class ProfilePage implements OnInit {
  lastImage: string = null;
  dataUser = {
    fullname: "",
    email: "",
    photo: "",
    uid: "",
    skill: ""
  };
  gambar ={
    base64Image:""
  }

  
  loading;
  pathPhoto;
  profileForm:FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheet: ActionSheetController, private camera:Camera,public toastCtrl: ToastController, public platform: Platform, public loadingCtrl: LoadingController, public afAuth: AngularFireAuth,  public afd: AngularFireDatabase, public afs: AngularFireStorage){
  
  
    this.afAuth.auth.onAuthStateChanged(user => {
    

      console.log(user);
      var userId = this.afAuth.auth.currentUser.uid;
      this.afd
        .list("users", ref => ref.orderByChild("uid").equalTo(userId))
        .valueChanges()
        .subscribe(data => {
          let tempDataUser = data[0];
          this.dataUser.fullname = tempDataUser["fullname"];
          this.dataUser.email = tempDataUser["email"];
          this.dataUser.uid = tempDataUser["uid"];
          this.dataUser.photo = tempDataUser["photo"];
          this.dataUser.skill = tempDataUser["skill"];         
          this.gambar.base64Image = tempDataUser["photo"];         
    });
  });
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  ngOnInit() {  
    this.initializeForm();
  }

  takePicture(source){
    var options= {
      quality: 100,
      sourceType: source,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.gambar.base64Image = `data:image/jpeg;base64,${imageData}`;
      
     }, (err) => {
      // Handle error
     }); 
  }

  loader(){
    this.loading = this.loadingCtrl.create({
      spinner: 'bubbles',
      content: 'Uploading...',
    });
    this.loading.present();
  }

  updateDenganPhoto(){
    this.loader();
    var path = "users/" + this.dataUser.uid + "/profilePicture";
    const ref = this.afs.ref(path)
    ref.putString(this.gambar.base64Image, "data_url").then(data=>{
     // this.pathPhoto = data.downloadURL;
     
      data.ref.getDownloadURL().then(url =>{
        //this.presentToast(url)
      //console.log(task)

   //  this.loading.dismiss();
     // return task;
      //console.log(this.pathPhoto);
      this.afd.list('users/').update(this.dataUser.uid,{
        fullname: this.profileForm.value.username,
        email: this.profileForm.value.email,
        photo: url,
        skill: this.profileForm.value.skill,
        gender: this.profileForm.value.gender
        
      }).then(data => {
        this.loading.dismiss();
        
      }).catch(err => {
        this.presentToast(err)
      })

    })
    }).catch(error=>{
      this.presentToast(error)
     // return task;
     // this.loading.dismiss();
    })
   // return task;
  }

  updateTanpaPhoto(){
    this.loader();
    this.afd.list('users/').update(this.dataUser.uid,{
      fullname: this.profileForm.value.username,
      email: this.profileForm.value.email,
      skill: this.profileForm.value.skill,
      gender: this.profileForm.value.gender
      
    }).then(data => {
      this.loading.dismiss();
      
    }).catch(err => {
      this.presentToast(err)
    })
  }

  presentActionSheet() {
    let actionSheet = this.actionSheet.create({
      buttons: [
        {
          text: 'Take a picture',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Get From gallery',
          handler: () => {
           this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
 
    actionSheet.present();
  }

  presentToast2(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 1000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 30000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  private initializeForm() {
    this.profileForm = new FormGroup({
      username: new FormControl(null,Validators.required),
      email: new FormControl(null,[Validators.required,Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]),
      gender: new FormControl("pria", Validators.required),
      skill: new FormControl(null)     
    })
  }



  uploadImg(){
    this.presentActionSheet();
  }

  editProfile(){

    if(this.gambar.base64Image != this.dataUser.photo){
      console.log("masuk dengan foto");
      this.updateDenganPhoto();
    }else{
      console.log("masuk tidak dengan foto");
      this.updateTanpaPhoto();
    }
  } 
}
