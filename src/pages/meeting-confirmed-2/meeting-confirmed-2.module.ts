import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeetingConfirmed_2Page } from './meeting-confirmed-2';

@NgModule({
  declarations: [
    MeetingConfirmed_2Page,
  ],
  imports: [
    IonicPageModule.forChild(MeetingConfirmed_2Page),
  ],
})
export class MeetingConfirmed_2PageModule {}
