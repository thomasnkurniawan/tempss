import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailMyProjectPage } from './detail-my-project';

@NgModule({
  declarations: [
    DetailMyProjectPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailMyProjectPage),
  ],
})
export class DetailMyProjectPageModule {}
