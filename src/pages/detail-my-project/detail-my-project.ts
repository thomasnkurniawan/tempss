import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireAuth } from "angularfire2/auth";

/**
 * Generated class for the DetailMyProjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-detail-my-project",
  templateUrl: "detail-my-project.html"
})
export class DetailMyProjectPage {
  memberReq = [];
  memberAcc = [];
  data = {
    id: "",
    kategori: "",
    keterangan: "",
    
    projectName: "",
    uid:"",
  };

  member = [];
  currUser;

  userjoin = "reqaccept";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public afd: AngularFireDatabase,
    public afauth: AngularFireAuth
  ) {
    this.data = this.navParams.get("Project");
    console.log(this.data, 'NAV PARAM PROJ');

    this.afd.list('users', ref=>ref.orderByChild('uid').equalTo(this.afauth.auth.currentUser.uid)).valueChanges().subscribe(data => {
      this.currUser = data[0];
    })

    this.fetch();
  }

  fetch() {
    this.memberAcc = []
    this.memberReq = []
    this.afd
      .list("project/" + this.data.id + "/member/")
      .valueChanges()
      .subscribe(data => {
        this.member = data;

        //this.member = this.data['member']
        console.log(this.member);
        for (let memberid in this.member) {
          if (this.member[memberid].status == 0) {
            this.memberReq.push(this.member[memberid]);
          } else {
            this.memberAcc.push(this.member[memberid]);
          }
        }
      });

    console.log(this.memberReq, "INI MEMBER YANG REQUEST JOIN KE PROJECT");
    console.log(this.memberAcc, "INI MEMBER YANG UDA DI PROJECT");
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad DetailMyProjectPage");
  }

  accept(memberacc) {
    console.log(memberacc, "MEMBER ACC");
    this.afd
      .list("project/" + this.data.id + "/member/")
      .update(memberacc.id, {
        status: 1
      })
      .then(() => {
        this.afd
          .list("users/" + memberacc.userId + "/joinProject/")
          .update(this.data.id, {
            status: 1
          })
          .then(() => {
            //this.navCtrl.pop()
            this.fetch();
          });
      });
  }
  reject(memberreject) {
    console.log(memberreject, "MEMBER ACC");
    this.afd
      .list("project/" + this.data.id + "/member/")
      .update(memberreject.id, {
        status: 0
      })
      .then(() => {
        this.afd
          .list("users/" + memberreject.userId + "/joinProject/")
          .update(this.data.id, {
            status: 0
          })
          .then(() => {
            this.fetch();
          });
      });
  }

  masukRoom(dataMember){
    this.navCtrl.push("ChattingPage",{
      username: this.currUser.fullname,
      dataMember: dataMember,
      roomId: dataMember.roomId,
      flag:'lanjutchat',
      idproject: this.data.id
    });
  }

  chatting(dataMember){

    this.navCtrl.push("ChattingPage",{
      username: this.currUser.fullname,
      dataMember: dataMember,
      flag:'create',
      idproject: this.data.id,
      roomId: dataMember.roomId,
      project: this.data
    });
  }

  lihatProfil(dataMember){
    this.navCtrl.push("LihatprofilePage",{
      b:dataMember
      
    })
  }
}
