import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListProjectPage } from './list-project';

@NgModule({
  declarations: [
    ListProjectPage,
  ],
  imports: [
    IonicPageModule.forChild(ListProjectPage),
  ],
})
export class ListProjectPageModule {}
