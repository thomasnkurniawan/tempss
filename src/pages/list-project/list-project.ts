import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';

/**
 * Generated class for the ListProjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-project',
  templateUrl: 'list-project.html',
})
export class ListProjectPage {
  categoryProj;
  listKategory;
  constructor(public navCtrl: NavController, public navParams: NavParams, public afd:AngularFireDatabase) {
    this.categoryProj = this.navParams.get('categoryProj')
    console.log(this.categoryProj)
    this.afd.list("project/", ref => ref.orderByChild('kategori').equalTo(this.categoryProj.category)).valueChanges().subscribe(data =>{
      this.listKategory = data;
      console.log(data)
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListProjectPage');
  }

  detailProject(detail){
    this.navCtrl.push("DetailProjectPage",{ 
      Detail:detail, 
      params: "home"
    });
  }

}
