import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the MapsModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-maps-modal',
  templateUrl: 'maps-modal.html',
})
export class MapsModalPage {
  // marker: Loc;
  // lat = -6.178306;
  // lng = 106.631889;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController) {
    // this.marker = new Loc(this.lat, this.lng);
  }

  ionViewDidLoad() {
    console.log('ionViewDiklkklodLoad MapsModalPage');
  }

  set_maps_clicked(){
    this.viewCtrl.dismiss();
  }
  // onSetMarker(event: any) {
  //   this.marker = new Loc(event.coords.lat, event.coords.lng);
  // }

}
