import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProjectService } from '../../services/project.service';
import { AngularFireAuth } from 'angularfire2/auth';
import kategori from '../../data/kategori';

@IonicPage()
@Component({
  selector: 'page-add-project',
  templateUrl: 'add-project.html',
})
export class AddProjectPage {
  submitProject: FormGroup;
  kategoriCollection: {category: string, image: string}[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public projectService: ProjectService, public afAuth: AngularFireAuth) {
    this.kategoriCollection = kategori
      this.initForm();
    
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddProjectPage');
  }

  submit(){
    console.log(this.submitProject.value);
    var uid = this.afAuth.auth.currentUser.uid;
    
    this.projectService.createProject(this.submitProject.value, uid)
    .then(data=>{
      console.log("input sukses");
    });
    this.viewCtrl.dismiss();
  }


  closeModal(){
    this.viewCtrl.dismiss();
  }
  initForm() {
    this.submitProject = new FormGroup({
      projectName: new FormControl(null, Validators.required),
      keterangan: new FormControl(null, Validators.required),
      kategori: new FormControl(null, Validators.required)
    });
  }
}
