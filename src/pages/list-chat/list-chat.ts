import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, LoadingController } from "ionic-angular";
import { connectableObservableDescriptor } from "rxjs/internal/observable/ConnectableObservable";
import { AngularFireDatabase } from "angularfire2/database";

/**
 * Generated class for the ListChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-list-chat",
  templateUrl: "list-chat.html"
})
export class ListChatPage {
  lstProject;
  lstRoomId = [];
  lstRoomChat = [];
  currName;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public afd: AngularFireDatabase
  ) {
    this.lstRoomChat = []
  
    //this.fetchRoomChat();
  }
  loading;
  presentLoading(){
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait..'
    })
    this.loading.present()
  }

  ionViewWillEnter() {
    
    this.fetchRoomChat()
  }


  fetchRoomChat() {
    this.presentLoading()
    this.lstRoomChat = []
    this.lstRoomId = [];
    this.lstProject = this.navParams.get("listProject");
    console.log(this.lstProject);
    this.currName = this.lstProject.fullname
    for (let proj in this.lstProject.joinProject) {
      if (this.lstProject.joinProject[proj].roomId) {
        this.lstRoomId.push(this.lstProject.joinProject[proj]);
        console.log("asdasdads");
      }
    }
    for (let proj2 in this.lstProject.project) {
      if (this.lstProject.project[proj2].roomId) {
        this.lstRoomId.push(this.lstProject.project[proj2]);
      }
    }
    console.log(this.lstRoomId, "INI CUMA ROOM ID");
    this.lstRoomId.forEach(element => {
      this.afd
        .list("chat", ref => ref.orderByChild("roomId").equalTo(element.roomId))
        .valueChanges()
        .subscribe(dataChat => {
          console.log(dataChat)
          this.lstRoomChat.push(dataChat[0]);
         
        });

    });
    this.loading.dismiss()
    console.log(this.lstRoomChat, "INI UDAH JADI LIST ROOMCHAT");
  }

  ionViewDidLoad() {
    
    console.log("ionViewDidLoad ListChatPage");
  }

  goToChat(roomChat){
  
    this.navCtrl.push("ChattingPage",{
      username: this.lstProject.fullname,
   
      roomId: roomChat.roomId,
      flag:'lanjutchat',
    });
  }
}
