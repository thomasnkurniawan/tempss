import { Component, OnInit, ViewChild } from "@angular/core";
import {
  NavController,
  IonicPage,
  LoadingController,
  ToastController,
  Content
} from "ionic-angular";
import kategori from "../../data/kategori";
import { AllKategori } from "../../services/kategoriService";
import { AuthService } from "../../services/authService";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireAuth } from "angularfire2/auth";
import { tap } from "rxjs/operators";
import { FcmProvider } from "../../providers/fcm/fcm";
@IonicPage()
@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage implements OnInit {
 
  currUser;
  userLst;
  items;
  load;
  kategoriCollection: { category: string; image: string }[];
  constructor(
    public navCtrl: NavController,
    public afauth: AngularFireAuth,
    private toast: ToastController,
    private authService: AuthService,
    public afd: AngularFireDatabase,
    public loadingCtrl: LoadingController,
    private allKategori: AllKategori,
    public fcm: FcmProvider
  ) {
   
    this.userLst = this.afd.list("users", ref =>
      ref.orderByChild("uid").equalTo(this.afauth.auth.currentUser.uid)
    );
    this.userLst.valueChanges().subscribe(data => {
      this.currUser = data[0];
      console.log(this.currUser, "INI CURR USER DATA");
    });
   
  }
  ngOnInit() {
    this.kategoriCollection = this.allKategori.getAllKategori();
    console.log(this.kategoriCollection, "INI KATEGORI");
   // this.load.dismiss()
   

    // this.fcm.listenToNotifications().pipe(
    //   tap(msg => {
    //     const t = this.toast.create({
    //       message: msg.body,
    //       duration: 3000
    //     })
    //     t.present();
    //   })
    // ).subscribe()
  }

 

  // ionViewDidLoad() {
  //   this.afd
  //     .list("categories/")
  //     .valueChanges()
  //     .subscribe(data => {
  //       const result = JSON.parse(data[0]["name"]);
  //       this.items = result;
  //       console.log(this.items)
  //       this.load.dismiss()
  //     });
  // }

  listProject(category) {
    console.log(category);
    this.navCtrl.push("ListProjectPage", {
      categoryProj: category
    });
  }
  logout() {
    this.authService.logout().then(data => {
      this.navCtrl.setRoot("LoginPage");
    });
  }
  listChat() {
    this.navCtrl.push("ListChatPage", {
      listProject: this.currUser
    });
  }
}
