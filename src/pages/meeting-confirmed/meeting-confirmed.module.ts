import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeetingConfirmedPage } from './meeting-confirmed';

@NgModule({
  declarations: [
    MeetingConfirmedPage,
  ],
  imports: [
    IonicPageModule.forChild(MeetingConfirmedPage),
  ],
})
export class MeetingConfirmedPageModule {}
