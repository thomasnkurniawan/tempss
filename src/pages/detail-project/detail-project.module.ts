import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailProjectPage } from './detail-project';

@NgModule({
  declarations: [
    DetailProjectPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailProjectPage),
  ],
})
export class DetailProjectPageModule {}
