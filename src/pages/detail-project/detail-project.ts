import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";

/**
 * Generated class for the DetailProjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-detail-project",
  templateUrl: "detail-project.html"
})
export class DetailProjectPage {
  data = {
    kategori: "",
    projectName: "",
    keterangan: ""
  };
  udahjoin: boolean = false
  uidCurrUser;
  // dataUser ={
  //   about: "",
  //   date: "",
  //   email:"",
  //   fullname:"",
  //   gender:"",
  //   photo:"",
  // }
  dataUser
  param

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public afAuth: AngularFireAuth,
    public afd: AngularFireDatabase
  ) {
    this.data = this.navParams.get("Detail");
    this.param = this.navParams.get('params')
    console.log(this.param, 'param')
    this.uidCurrUser = this.afAuth.auth.currentUser.uid
    this.afd.list("users/", ref=> ref.orderByChild('uid').equalTo(this.uidCurrUser)).valueChanges().subscribe(data=>{
      this.dataUser = data[0];
      console.log(this.dataUser,"tittit");
    })
    
    console.log(this.data);
    
     this.afd.list("project/" + this.data['id'] + "/member/").valueChanges().subscribe(dataUserJoin => {
    console.log(dataUserJoin, 'INI USER YANG JOIN KE PROJEK')
    for(let user in dataUserJoin){
      if(dataUserJoin[user]['userId'] == this.uidCurrUser){
        this.udahjoin = true
      }
    }
  })





  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad DetailProjectPage");
  }

  joinProject() {
    this.udahjoin = true;
    console.log("ngen");
    var user = this.afAuth.auth.currentUser.uid;
    
    var memberList = this.afd.list("project/" + this.data["id"] + "/member/");
    const memberRef = memberList.push({});
    memberRef
      .set({
        userId: user,
        fullname: this.dataUser.fullname,
        email: this.dataUser.email,
        status: 0,
        id: memberRef.key
      })
      .then(() => {
        console.log("input data ")
        const userList = this.afd.list("users/" + user + "/joinProject/");
        userList.set(this.data["id"], {
          projectId: this.data["id"],
          status: 0
        });
      });
      this.navCtrl.push("MeetingPage");
    
    
  }
  chatPage(){
    this.navCtrl.push("ChatPage");
  }
}
