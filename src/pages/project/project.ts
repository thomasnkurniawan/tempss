import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  //Nav,
  ModalController
} from "ionic-angular";
import { AngularFireDatabase } from "angularfire2/database";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Storage } from "@ionic/storage";
import { AngularFireAuth } from "angularfire2/auth";
/**
 * Generated class for the ProjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-project",
  templateUrl: "project.html"
})
export class ProjectPage {
  userForm: FormGroup;
  myProject = [];
  projectSegment = "myProject";
  joinProject =[];

  projJoin = {
    projectName: "",
    keterangan: "",
    kategori: "",
    status: ""
  };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public afd: AngularFireDatabase,
    private storage: Storage,
    //private nav: Nav,
    public modalCtrl: ModalController,
    public afAuth: AngularFireAuth
  ) {
    var uid = this.afAuth.auth.currentUser.uid;
    console.log(uid);
    this.afd
      .list("project/", ref => ref.orderByChild("uid").equalTo(uid))
      .valueChanges()
      .subscribe(data => {
        this.myProject = data;
        console.log(this.myProject);
      });

    this.afd
      .list("users/" + uid + "/joinProject/")
      .valueChanges()
      .subscribe(jonpr => {
        console.log(jonpr, "INI PROJ JOIN");
        this.joinProject = jonpr;
        this.joinProject.forEach(element => {
          this.afd
            .list("project/", ref => ref.orderByChild("id").equalTo(element.projectId))
            .valueChanges()
            .subscribe(dtproj => {
              console.log(dtproj)
              
              this.projJoin.kategori = dtproj[0]['kategori'];
              this.projJoin.keterangan = dtproj[0]['keterangan'];
              this.projJoin.projectName = dtproj[0]['projectName'];
              this.projJoin.status = element.status;
              this.joinProject.push(this.projJoin);
              this.projJoin = {
                projectName: "",
                keterangan: "",
                kategori: "",
                status: ""
              }

            });
        });
      });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ProjectPage");
  }

  ngOnInit() {
    this.initializeForm();
  }

  private initializeForm() {
    this.userForm = new FormGroup({
      projectname: new FormControl(null, Validators.required)
    });
  }

  onSubmit() {
    this.storage.get("uid").then(data => {
      const result = <any>{};
      result.uid = data;
      result.project = this.userForm.value;
      this.afd.list("/project/").push(result);
    });
    this.navCtrl.push("HomePage");
  }

  addProPage() {
    const modal = this.modalCtrl.create("AddProjectPage");
    modal.present();
  }

  detail(project) {
    console.log(project);
    this.navCtrl.push("DetailMyProjectPage", { Project: project});
  }
  detailJoin(detail){
    console.log(detail,"ini detail project");
    this.navCtrl.push("DetailProjectPage",{Detail:detail,param:"joinProject"});
    
  }

}
