import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '../../services/authService';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage implements OnInit{
  userForm:FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,private authService:AuthService,private toast:ToastController,private storage: Storage) {

  }

  presentToast() {
    const toast = this.toast.create({
      message: 'Login Berhasil',
      duration: 3000
    });
    toast.present();
  }

  failedToast() {
    const toast = this.toast.create({
      message: 'Login Gagal',
      duration: 3000
    });
    toast.present();
  }

  ngOnInit() {  
    this.initializeForm();
  }

  private initializeForm() {
    this.userForm = new FormGroup({
      email: new FormControl(null,[Validators.required,Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]),
      password: new FormControl(null,[Validators.required,Validators.minLength(8)])
    })
  }

  login(){
    this.authService.signin(this.userForm.value.email, this.userForm.value.password).then(data=>{
      this.storage.set('uid',data.user.uid)
      this.navCtrl.setRoot("HomePage")
      this.presentToast();
      console.log(data);
    }).catch(error=>{
      console.log(error);
      this.failedToast();
    })
  }

  goToSignUp(){
    this.navCtrl.setRoot("SignUpPage");
  }

}
