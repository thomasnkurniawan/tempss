import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators, } from '@angular/forms';
import { AngularFireDatabase } from 'angularfire2/database';
import firebase from 'firebase';

/**
 * Generated class for the SkripsiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignUpPage implements OnInit{
  
  userForm:FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public afd: AngularFireDatabase) {
  }

  ngOnInit() {  
    this.initializeForm();
  }

  private initializeForm() {
    this.userForm = new FormGroup({
      fullname: new FormControl(null,Validators.required),
      email: new FormControl(null,[Validators.required,Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]),
      password: new FormControl(null,[Validators.required,Validators.minLength(8)])
    })
  }

  onSubmit() {
      firebase.auth().createUserWithEmailAndPassword(this.userForm.value.email,this.userForm.value.password).then(data => {
        let result = <any>{};
        
        result = this.userForm.value;
        
        console.log("teseteste ", data.user.uid)
        const projectRef = this.afd.list("users/");
            projectRef.set(data.user.uid,{
                uid:data.user.uid,
                fullname:result.fullname,
                email:result.email,
              }).then(()=>{
                console.log("wantotbangsat")
                firebase.auth().currentUser.sendEmailVerification().then(function() {
                }, function(error) {
                console.log(error)
                });
              })
        });
        this.navCtrl.push("LoginPage");
  }

  ionViewDidLoad() {
  }

  goToLogin(){
    this.navCtrl.setRoot("LoginPage");
  }

}
