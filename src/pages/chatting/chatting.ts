import { Component, ViewChild } from "@angular/core";
import { IonicPage, NavController, NavParams, Content } from "ionic-angular";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireAuth } from "angularfire2/auth";
import { HttpHeaders, HttpClient } from "@angular/common/http";

/**
 * Generated class for the ChattingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-chatting",
  templateUrl: "chatting.html"
})
export class ChattingPage {
  @ViewChild(Content) content: Content;

  username: string = "";
  message: string = "";
  _chatSubscription;
  messages: object[] = [];
  roomId;
  currProject;
  constructor(
    public db: AngularFireDatabase,
    public navCtrl: NavController,
    public navParams: NavParams,
    
    public afauth: AngularFireAuth
  ) {
    this.username = this.navParams.get("username");
    this.currProject = this.navParams.get('project');
    this.roomId = this.navParams.get('roomId');
    console.log(this.roomId);
    this.scrollToTop()
  }

  scrollToTop() {
    this.content.scrollToBottom();
  }


  fetch() {
    console.log(this.roomId);
    this._chatSubscription = this.db
      .list("/chat/" + this.roomId + "/messages")
      .valueChanges()
      .subscribe(data => {
        console.log(data);
        this.messages = data;
      });
  }

  sendMessage() {
    this.db
      .list("/chat/" + this.roomId + "/messages/")
      .push({
        username: this.username,
        message: this.message
      })
      .then(() => {
        
        this.fetch();
        this.scrollToTop()
       
      });
    this.message = "";
  }
  chatTo;
  idProject;
  ionViewDidLoad() {
    let param = this.navParams.get("flag");
    if (param == "create") {
      const ref = this.db.list("/chat").push({});
      ref
        .set({
          roomId: ref.key,
          specialMessage: true,
          messages: `${this.username} has joined the room`,
          bosName: this.username,
          projectName: this.currProject.projectName,
          karyawanName: this.navParams.get('dataMember').fullname
        })
        .then(() => {
          this.roomId = ref.key;
          this.chatTo = this.navParams.get("dataMember");
          this.idProject = this.navParams.get("idproject");
          console.log(
            this.idProject,
            "INI ID PROJECT",
            this.chatTo.userId,
            "INI UID TO",
            this.roomId,
            "INI ROOM ID"
          );
          const refs = this.db.list(
            "users/" + this.chatTo.userId + "/joinProject/"
          );
          refs.update(this.idProject, { roomId: ref.key }).then(() => {
            const refsTo = this.db.list(
              "project/" + this.idProject + "/member/"
            );
            refsTo.update(this.chatTo.id, { roomId: ref.key }).then(() =>{
              const refsFrom = this.db.list(
                "users/" + this.afauth.auth.currentUser.uid + "/project/"
              );
              refsFrom.update(this.idProject, {roomId: this.roomId})
            });
          });
        });
    } else {
      this.roomId = this.navParams.get("roomId");
      this.fetch();
    }
  }

  ionViewWillLeave() {
    // this._chatSubscription.unsubscribe();
    // this.db.list('/chat').push({
    //   specialMessage: true,
    //   message: `${this.username} has left the room`
    // });
  }
}
