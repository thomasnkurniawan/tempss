import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { GooglePlus } from '@ionic-native/google-plus';
import { FirebaseConf } from '../config';
import { AuthService } from '../services/authService';
import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { FilePath } from '@ionic-native/file-path';
import { ProjectService } from '../services/project.service';
import { AgmCoreModule } from '@agm/core';
import { MapsModalPage } from '../pages/maps-modal/maps-modal';
import { AllKategori } from '../services/kategoriService';
import { Push } from '@ionic-native/push';
import { Firebase } from '@ionic-native/firebase'
import { FCM } from '@ionic-native/fcm';
import { FcmProvider } from '../providers/fcm/fcm';

@NgModule({
  declarations: [
    MyApp,
    MapsModalPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireAuthModule,
    AngularFireModule.initializeApp(FirebaseConf.credential),
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    AgmCoreModule.forRoot({apiKey:"AIzaSyBERON_e07ms9SewlPReSEd2JYwiuWe-yI"}),
    AngularFirestoreModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MapsModalPage
  ],
  providers: [
    GooglePlus,
    FCM,
    StatusBar,
    SplashScreen,
    AuthService,
    ProjectService,
    AllKategori,
    Camera,
    File,
    FileTransfer,
    FilePath,
    Push,
    Firebase,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FcmProvider
  ]
})
export class AppModule {}
