import { Component,ViewChild  } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthService } from '../services/authService';
import { timer } from 'rxjs/observable/timer';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { FCM } from '@ionic-native/fcm';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage:any = "IntroductionPage";
  projectPage = "ProjectPage";
  profilePage = "ProfilePage";
  homePage = "HomePage";
  meetingPage = "MeetingPage";
  meetingConfirmed = "MeetingConfirmedPage";
  meetingConfirmed_2 = "MeetingConfirmed_2Page";
  
  @ViewChild(Nav) nav: Nav;
  dataUser = {
    fullname: "",
    email: "",
    photo: "",
    uid: ""
  };
  showSplash = true;
  constructor(
    platform: Platform,
    statusBar: StatusBar,
    public afAuth: AngularFireAuth,
    splashScreen: SplashScreen,
    private authService: AuthService,
    public afd: AngularFireDatabase,
    public fcm: FCM
  ){
   // platform.ready().then(() => {
      // firebase.initializeApp(Firebase.credential);
      // const unsubscribe = firebase.auth().onAuthStateChanged(user => {
      //   if(!user) {
      //     this.rootPage = "IntroductionPage";
      //     unsubscribe();
      //   }
      platform.ready().then(() => {
        //   firebase.initializeApp(Firebase.credential);
        this.afAuth.auth.onAuthStateChanged(user => {
          if (!user) {
            this.rootPage = "IntroductionPage";
            //          unsubscribe();
          }
        else {

          console.log(user);
          var userId = this.afAuth.auth.currentUser.uid;
          this.afd
            .list("users", ref => ref.orderByChild("uid").equalTo(userId))
            .valueChanges()
            .subscribe(data => {
              let tempDataUser = data[0];
              
              this.dataUser.fullname = tempDataUser["fullname"];
              this.dataUser.email = tempDataUser["email"];
              this.dataUser.uid = tempDataUser["uid"];

               this.dataUser.photo = tempDataUser["photo"];

               this.fcm.subscribeToTopic(tempDataUser['uid']);
               this.fcm.subscribeToTopic('all');
              
              this.rootPage = "HomePage";
            });
        }
      });
      statusBar.styleDefault();
      splashScreen.hide();
      timer(3000).subscribe(() => this.showSplash = false);
    });
  }

  logout() {
    this.authService.logout().then(data => this.nav.setRoot("LoginPage"))
  }

  openPage(page:any) {
  this.nav.setRoot(page); 
  }

  profile(){
    this.nav.push("ProfilePage");
  }
}

