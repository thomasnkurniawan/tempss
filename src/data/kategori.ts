export default [
    {
      category: 'Website Developer',
      image: '../../assets/imgs/web.jpg'
    },
    {
      category: 'Mobile Developer',
      image: '../../assets/imgs/mob.jpg'
    },
    {
      category: 'Design Grafis',
      image: '../../assets/imgs/ui.jpg'
    },
    {
      category: 'Content Writer',
      image: '../../assets/imgs/content.jpg'
    },
    {
      category: 'Digital Marketing',
      image: '../../assets/imgs/marketing.jpg'
    },
    {
      category: 'Lain-lain',
      image: '../../assets/imgs/marketing.jpg'
    }
  ];
  